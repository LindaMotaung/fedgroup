﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxApplication
{
    public partial class frmTaxApplication : Form
    {
        private bool isSalaryTextboxTouched = false;
        private bool isAgeTextboxTouched = false;
        public frmTaxApplication()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            int ageInput;
            bool ageIsValid = false;
            int salaryInput;
            bool salaryIsValid = false;
            int salaryIntValue = Convert.ToInt32(txtSalaryPerAnnum.Text);
            int ageIntValue = Convert.ToInt32(txtAge.Text);
            double taxAmount = 0;

            if (int.TryParse(txtSalaryPerAnnum.Text, out salaryInput))
                salaryIsValid = true;
            else
                MessageBox.Show("Please enter a valid numeric integer value for the Salary field.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

            if (int.TryParse(txtAge.Text, out ageInput))
                ageIsValid = true;
            else
                MessageBox.Show("Please enter a valid numeric integer value for the Age field.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

  

            if (salaryIsValid == true && ageIsValid == true)
            {
                if (Convert.ToInt32(txtAge.Text) < 18)
                    MessageBox.Show("Please enter an Age value greater than 18.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);           
                else
                {
                    //Then validation has passed
                    if (salaryIntValue >= 0 && salaryIntValue <= 5000)
                    {
                        if (ageIntValue >= 18 && ageIntValue <= 50)
                        {
                            lblTaxAmount.Visible = true;
                            salaryIntValue = salaryIntValue - 2000; //R2000 relief 
                            taxAmount = salaryIntValue * 1; //100% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else if (ageIntValue > 50)
                        {
                            lblTaxAmount.Visible = true;
                            salaryIntValue = salaryIntValue - 5000; //R5000 relief 
                            taxAmount = salaryIntValue * 0.85; //85% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else
                        {
                            lblTaxAmount.Visible = true;
                            lblTaxAmount.Text = "R0.00";
                        }
                    }
                    else if (salaryIntValue >= 5000 && salaryIntValue <= 10000)
                    {
                        if (ageIntValue >= 18 && ageIntValue <= 50)
                        {
                            lblTaxAmount.Visible = true;
                            salaryIntValue = salaryIntValue - 2000; //R2000 relief 
                            taxAmount = salaryIntValue * 1; //100% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else if (ageIntValue > 50)
                        {
                            lblTaxAmount.Visible = true;
                            salaryIntValue = salaryIntValue - 5000; //R5000 relief 
                            taxAmount = salaryIntValue * 0.85; //85% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else
                        {
                            lblTaxAmount.Visible = true;
                            //taxAmount = salaryIntValue * (5 / 100);
                            taxAmount = salaryIntValue * 0.05; //5%
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                    }
                    else if (salaryIntValue >= 10000 && salaryIntValue <= 20000)
                    {
                        if (ageIntValue >= 18 && ageIntValue <= 50)
                        {
                            lblTaxAmount.Visible = true;
                            salaryIntValue = salaryIntValue - 2000; //R2000 relief 
                            taxAmount = salaryIntValue * 1; //100% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else if (ageIntValue > 50)
                        {
                            lblTaxAmount.Visible = true;
                            salaryIntValue = salaryIntValue - 5000; //R5000 relief 
                            taxAmount = salaryIntValue * 0.85; //85% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else
                        {
                            lblTaxAmount.Visible = true;
                            taxAmount = salaryIntValue * 0.075; //7.5%
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                    }
                    else if (salaryIntValue >= 20000 && salaryIntValue <= 35000)
                    {
                        if (ageIntValue >= 18 && ageIntValue <= 50)
                        {
                            lblTaxAmount.Visible = true;
                            salaryIntValue = salaryIntValue - 2000; //R2000 relief 
                            taxAmount = salaryIntValue * 1; //100% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else if (ageIntValue > 50)
                        {
                            lblTaxAmount.Visible = true;
                            salaryIntValue = salaryIntValue - 5000; //R5000 relief 
                            taxAmount = salaryIntValue * 0.85; //85% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else
                        {
                            lblTaxAmount.Visible = true;
                            taxAmount = salaryIntValue * 0.09; //9%
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                    }
                    else if (salaryIntValue >= 35000 && salaryIntValue <= 50000)
                    {
                        if (ageIntValue >= 18 && ageIntValue <= 50)
                        {
                            salaryIntValue = salaryIntValue - 2000; //R2000 relief 
                            taxAmount = salaryIntValue * 1; //100% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else if (ageIntValue > 50)
                        {
                            salaryIntValue = salaryIntValue - 5000; //R5000 relief 
                            taxAmount = salaryIntValue * 0.85; //85% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else
                        {
                            lblTaxAmount.Visible = true;
                            taxAmount = salaryIntValue * 0.15; //15%
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                    }
                    else if (salaryIntValue >= 50000 && salaryIntValue <= 70000)
                    {
                        if (ageIntValue >= 18 && ageIntValue <= 50)
                        {
                            salaryIntValue = salaryIntValue - 2000; //R2000 relief 
                            taxAmount = salaryIntValue * 1; //100% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else if (ageIntValue > 50)
                        {
                            salaryIntValue = salaryIntValue - 5000; //R5000 relief 
                            taxAmount = salaryIntValue * 0.85; //85% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else
                        {
                            lblTaxAmount.Visible = true;
                            taxAmount = salaryIntValue * 0.25; //25%
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                    }
                    else if (salaryIntValue > 70000)
                    {
                        if (ageIntValue >= 18 && ageIntValue <= 50)
                        {
                            salaryIntValue = salaryIntValue - 2000; //R2000 relief 
                            taxAmount = salaryIntValue * 1; //100% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else if (ageIntValue > 50)
                        {
                            salaryIntValue = salaryIntValue - 5000; //R5000 relief 
                            taxAmount = salaryIntValue * 0.85; //85% deduction
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                        else
                        {
                            lblTaxAmount.Visible = true;
                            taxAmount = salaryIntValue * 0.3; //25%
                            lblTaxAmount.Text = "R" + Convert.ToString(taxAmount) + ".00";
                        }
                    }
                }
            }     
        }

        private void frmTaxApplication_Load(object sender, EventArgs e)
        {
            btnCalculate.Enabled = false;
            this.ActiveControl = txtSalaryPerAnnum;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MinimizeBox = true;
            this.MaximizeBox = false;
            this.HelpButton = false;
            this.AutoSize = true;
        }

        private void txtSalaryPerAnnum_TextChanged(object sender, EventArgs e)
        {
            isSalaryTextboxTouched = true;
        }

        private void txtAge_TextChanged(object sender, EventArgs e)
        {
            isAgeTextboxTouched = true;
            if ((isSalaryTextboxTouched == true && isAgeTextboxTouched  == true)
                && (txtSalaryPerAnnum.Text != string.Empty && txtAge.Text != string.Empty))
            {
                btnCalculate.Enabled = true;
            }
        }
    }
}
