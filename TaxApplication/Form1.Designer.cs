﻿namespace TaxApplication
{
    partial class frmTaxApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSalaryPerAnnum = new System.Windows.Forms.TextBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.lblSalary = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblAnnualTax = new System.Windows.Forms.Label();
            this.lblTaxAmount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtSalaryPerAnnum
            // 
            this.txtSalaryPerAnnum.Location = new System.Drawing.Point(56, 43);
            this.txtSalaryPerAnnum.Multiline = true;
            this.txtSalaryPerAnnum.Name = "txtSalaryPerAnnum";
            this.txtSalaryPerAnnum.Size = new System.Drawing.Size(198, 33);
            this.txtSalaryPerAnnum.TabIndex = 0;
            this.txtSalaryPerAnnum.TextChanged += new System.EventHandler(this.txtSalaryPerAnnum_TextChanged);
            // 
            // txtAge
            // 
            this.txtAge.Location = new System.Drawing.Point(56, 105);
            this.txtAge.Multiline = true;
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(198, 33);
            this.txtAge.TabIndex = 1;
            this.txtAge.TextChanged += new System.EventHandler(this.txtAge_TextChanged);
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(56, 153);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(198, 33);
            this.btnCalculate.TabIndex = 2;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // lblSalary
            // 
            this.lblSalary.AutoSize = true;
            this.lblSalary.Location = new System.Drawing.Point(93, 23);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(126, 17);
            this.lblSalary.TabIndex = 3;
            this.lblSalary.Text = "Salary Per Annum:";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(137, 85);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(37, 17);
            this.lblAge.TabIndex = 4;
            this.lblAge.Text = "Age:";
            // 
            // lblAnnualTax
            // 
            this.lblAnnualTax.AutoSize = true;
            this.lblAnnualTax.Location = new System.Drawing.Point(115, 199);
            this.lblAnnualTax.Name = "lblAnnualTax";
            this.lblAnnualTax.Size = new System.Drawing.Size(83, 17);
            this.lblAnnualTax.TabIndex = 5;
            this.lblAnnualTax.Text = "Annual Tax:";
            // 
            // lblTaxAmount
            // 
            this.lblTaxAmount.AutoSize = true;
            this.lblTaxAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxAmount.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblTaxAmount.Location = new System.Drawing.Point(91, 237);
            this.lblTaxAmount.Name = "lblTaxAmount";
            this.lblTaxAmount.Size = new System.Drawing.Size(80, 29);
            this.lblTaxAmount.TabIndex = 6;
            this.lblTaxAmount.Text = "Label";
            this.lblTaxAmount.Visible = false;
            // 
            // frmTaxApplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 301);
            this.Controls.Add(this.lblTaxAmount);
            this.Controls.Add(this.lblAnnualTax);
            this.Controls.Add(this.lblAge);
            this.Controls.Add(this.lblSalary);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.txtAge);
            this.Controls.Add(this.txtSalaryPerAnnum);
            this.Name = "frmTaxApplication";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tax Application";
            this.Load += new System.EventHandler(this.frmTaxApplication_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSalaryPerAnnum;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Label lblSalary;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label lblAnnualTax;
        private System.Windows.Forms.Label lblTaxAmount;
    }
}

